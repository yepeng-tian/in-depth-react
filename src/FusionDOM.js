import { createElement as create } from './dom'

// let fusionNode={
//   type: type, // String(TextNode) FusionComponent FusionNativeComponent
//   tag: 'div',
//   parent: 'fusionNode',
//   value: 'text',
//   attributes: {key: 'value'},
//   children: 'fusionNode'
// }

function FusionNode(type, tag, attributes, props) {
  return {
    type: type, // String(TextNode) FusionComponent FusionNativeComponent
    tag: tag,
    parent: null,
    attributes: attributes ? attributes : {},
    value: '',
    props: props ? props : {},
    children: null
  }
}
function FusionNativeComponent(nodeName, attributes, children) {
  attributes = attributes ? attributes : {}
  children = children ? children : []
  let node = new FusionNode(FusionNativeComponent, nodeName, attributes)
  node.children = children
}
function FusionTextComponent(text) {
  let node = new FusionNode(String, null, null)
  node.value = text
  node.attributes = {}
  node.children = []
}
// May need debug
function FusionComponent(nodeName, props, children) {
  props = props ? props : {}
  props.children = children ? children : []
  let node = new FusionNode(FusionComponent, nodeName, null, props)
  node.children = nodeName()
}

// Generate virtual DOM
function createElement(nodeName, attributes, ...args) {
  let children = []
  args.forEach(child => {
    if (typeof child === 'string') children.push(new FusionTextComponent(child))
    else children.push(child)
  })
  if (isNative(nodeName)) {
    let node = new FusionNativeComponent(nodeName, attributes, children)
    children.forEach(child => {
      child.parent = node
    })
    return node
  } else {
    let props = attributes
    props.children = args
    let node = new FusionComponent(nodeName, props)

    children.forEach(child => {
      child.parent = node
    })
    return node
  }
}

// Generate real DOM
function createDom(node, parent) {
  if (node.type === FusionComponent) {
    // TODO WRONG
    createDom(node.children[0], parent)
    for (i = 1; i < node.children.length; i++) {
      createDom(node.children[i], node.children[0])
    }
  } else {
    // FusionNativeComponent or FusionTextComponent
    parent.appendChild(create(node))
  }
}

function render(rootComponent, htmlElement) {
  // TODO
}

// May support custom HTML tag in future
let nativeTag = ["a", "abbr", "acronym", "address", "applet", "area", "article", "aside", "audio", "b", "base", "basefont", "bdi", "bdo", "big", "blockquote", "body", "br", "button", "canvas", "caption", "center", "cite", "code", "col", "colgroup", "data", "datalist", "dd", "del", "details", "dfn", "dialog", "dir", "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "font", "footer", "form", "frame", "frameset", "h1 to h6", "head", "header", "hr", "html", "i", "iframe", "img", "input", "ins", "kbd", "label", "legend", "li", "link", "main", "map", "mark", "meta", "meter", "nav", "noframes", "noscript", "object", "ol", "optgroup", "option", "output", "p", "param", "picture", "pre", "progress", "q", "rp", "rt", "ruby", "s", "samp", "script", "section", "select", "small", "source", "span", "strike", "strong", "style", "sub", "summary", "sup", "svg", "table", "tbody", "td", "template", "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "tt", "u", "ul", "var", "video", "wbr"]
function isNative(nodeName) {
  if (typeof nodeName === 'function') return false
  nodeName = nodeName.toLowerCase()
  if (nativeTag.indexOf(nodeName) !== -1) return true
  return false
}

export default createElement
export { render }